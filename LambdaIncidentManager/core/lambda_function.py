"""
This lambda function calls a private url to deploy k8s services (the lambda is configured with a private vpc)
The lambda is invoked from a runbook aws incident manager once an incident is created.

Author:
    Julian Perez Ramirez - julian.perez@payu.com

License:
    PayU Latam - Copyright (c) 2024
    http://www.payu.com
    Date: 02/07/24
"""

import json
import urllib3
import boto3

"""
metric_alarms = ["BabylonEventAlert", "CredibancoEventAlert"]
logGroupNames = {
    "BabylonEventAlert": "/aws/events/babylonAlert",
    "CredibancoEventAlert": "/aws/events/CredibancoAlert"
}
"""

metric_alarms = ["BabylonErrorLog", "CredibancoErrorLog"]
logGroupNames = {
    "BabylonErrorLog": "/aws/events/ryuk_rule_1",
    "CredibancoErrorLog": "/aws/events/ryuk_rule_2"
}


def lambda_handler(event: dict, context: dict) -> dict:
    log_events = get_log_events(state_alarm="INSUFFICIENT_DATA")
    urls = []
    http_codes = []
    data = []
    for event_msg in log_events:
        parameters = get_parameters_to_restart(message=event_msg)
        # url, response = call_k8s_deploy(event=event, parameters=parameters)
        # urls.append(url)
        # http_codes.append(response.status)
        # data.append(response.data.decode("utf-8"))
        urls.append(json.dumps(parameters))
        http_codes.append(200)
        data.append("updated")

    return {
        "web_url": urls,
        "http_code": http_codes,
        "data": data
    }


def call_k8s_deploy(event: dict, parameters: dict) -> tuple:
    headers = {
        "X-Email": event["X-Email"],
        "X-Username": event["X-Username"],
        "X-Auth-token": event["X-Auth-Token"],
        "Content-Type": "application/json"
    }

    data = {}

    url = ("http://k8s-deploy-app.gitlab-runners." + parameters["environment"] + "/services/" + parameters["account"] +
           "/" + parameters["cluster"] + "/" + parameters["namespace"] + "/" + parameters["service"])

    http = urllib3.PoolManager()
    response = http.request(method="PUT", url=url, headers=headers, body=json.dumps(data))

    return url, response


def get_log_events(state_alarm: str) -> list:
    region = "us-east-1"
    cloudwatch = boto3.client("cloudwatch", region_name=region)
    logs = boto3.client("logs", region_name=region)
    in_alarm: list = cloudwatch.describe_alarms(StateValue=state_alarm)["MetricAlarms"]
    component_info: list = []
    for alarm in in_alarm:
        for metric_alarm in metric_alarms:
            if alarm["AlarmName"] == metric_alarm:
                log_stream = logs.describe_log_streams(
                    logGroupName=logGroupNames[metric_alarm], orderBy="LastEventTime")["logStreams"][-1]
                component_info.append(json.loads(logs.get_log_events(
                    logGroupName=logGroupNames[metric_alarm],
                    logStreamName=log_stream["logStreamName"])["events"][0]["message"]))
    return component_info


def get_parameters_to_restart(message: dict) -> dict:
    component = message["detail"]["additional"]
    parameters: dict = {}
    if component["cluster"] == "payu-app-green-preprod" or component["cluster"] == "payu-app-blue-preprod":
        parameters.update({"environment": "payu.preprod"})
        parameters.update({"account": "aws-preprod"})
        parameters.update({"cluster": "app"})
    elif component["cluster"] == "payu-web-green-preprod" or component["cluster"] == "payu-web-blue-preprod":
        parameters.update({"environment": "payu.preprod"})
        parameters.update({"account": "aws-preprod"})
        parameters.update({"cluster": "web"})
    elif component["cluster"] == "payu-app-sandbox":
        parameters.update({"environment": "payu.sandbox"})
        parameters.update({"account": "aws-sandbox"})
        parameters.update({"cluster": "app"})
    elif component["cluster"] == "payu-web-blue-sandbox" or component["cluster"] == "payu-web-sandbox":
        parameters.update({"environment": "payu.sandbox"})
        parameters.update({"account": "aws-sandbox"})
        parameters.update({"cluster": "app"})
    elif component["cluster"] == "payu-app-green-prod" or component["cluster"] == "payu-app-blue-prod":
        parameters.update({"environment": "payu.prod"})
        parameters.update({"account": "aws-production"})
        parameters.update({"cluster": "app"})
    elif component["cluster"] == "payu-web-green-prod" or component["cluster"] == "payu-web-blue-prod":
        parameters.update({"environment": "payu.prod"})
        parameters.update({"account": "aws-production"})
        parameters.update({"cluster": "web"})

    parameters.update({"namespace": component["namespace"]})
    parameters.update({"service": component["component"]})

    return parameters