"""
Unit tests for the lambda k8s deploy function.
IMPORTANT: To run it, it must be connected to the vpn (due to the url to be reach is private)

Author:
    Julian Perez - julian.perez@payu.com

License:
    PayU Latam - Copyright (c) 2024
    http://www.payu.com
    Date: 02/07/24
"""
import pytest
import urllib3

from LambdaDeployK8s.core.lambda_function import call_k8s_deploy, get_parameters_to_restart


class TestCallWrongServiceName:

    @pytest.fixture
    def call_with_wrong_service_name(self) -> tuple:
        event = {
            "X-Email": "devopslatam@payu.com",
            "X-Username": "Incident Manager",
            "X-Auth-Token": "fyF6BTG4Xf3lwEI6MJg7"
        }
        parameters = {
            "environment": "payu.preprod",
            "account": "aws-preprod",
            "cluster": "app",
            "namespace": "adapters",
            "service": "fake"
        }
        return (str(parameters["namespace"] + ":" + parameters["service"]),
                call_k8s_deploy(event=event, parameters=parameters))

    def test_response_with_wrong_service_name(self, call_with_wrong_service_name: tuple) -> None:
        response: urllib3.PoolManager().request = call_with_wrong_service_name[1][1]
        assert response.status == 404
        assert (response.data.decode("utf-8") == "deployment configurations.yaml not found for service " +
                call_with_wrong_service_name[0])


class TestCallWrongToken:

    @pytest.fixture
    def call_with_wrong_token(self) -> tuple:
        event = {
            "X-Email": "devopslatam@payu.com",
            "X-Username": "Incident Manager",
            "X-Auth-Token": "fake"
        }
        parameters = {
            "environment": "payu.preprod",
            "account": "aws-preprod",
            "cluster": "app",
            "namespace": "adapters",
            "service": "transbank-debit-mall-adapter"
        }
        return call_k8s_deploy(event=event, parameters=parameters)

    def test_response_with_wrong_token(self, call_with_wrong_token: tuple) -> None:
        response: urllib3.PoolManager().request = call_with_wrong_token[1]
        assert response.status == 403
        assert response.data.decode("utf-8") == "Invalid authentication token"


class TestGetParameters:
    @pytest.fixture
    def call_get_parameters(self):
        message = {
            "version": "0",
            "id": "6769b54a-8f16-08b3-2b4c-f382b8c1085b",
            "detail-type": "alert",
            "source": "ryuk_app",
            "account": "813168479867",
            "time": "2024-07-02T21:15:44Z",
            "region": "us-east-1",
            "resources": [],
            "detail": {
                "name": "ApplicationContainerMemoryUsageCritical",
                "description": "Babylon error",
                "type": "applications",
                "severity": "critical",
                "additional": {
                    "cluster": "payu-app-green-preprod",
                    "component": "babylon",
                    "namespace": "houston"
                }
            }
        }
        return get_parameters_to_restart(message=message)

    def test_get_parameters_to_restart(self, call_get_parameters: dict):
        assert call_get_parameters["environment"] == "payu.preprod"
        assert call_get_parameters["account"] == "aws-preprod"
        assert call_get_parameters["cluster"] == "app"
        assert call_get_parameters["namespace"] == "houston"
        assert call_get_parameters["service"] == "babylon"
